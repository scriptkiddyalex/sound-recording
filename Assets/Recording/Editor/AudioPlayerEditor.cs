﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.SceneManagement;

[CustomEditor(typeof(AudioPlayer))]
public class AudioPlayerEditor : Editor {
	AudioPlayer _target;

	public override void OnInspectorGUI() {
		_target = (AudioPlayer)target;
		if (Application.isPlaying)
		{
			if (GUILayout.Button("Play"))
			{
				_target.Replay();
			}
		}
	}
}
