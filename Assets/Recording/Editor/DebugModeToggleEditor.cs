﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(DebugModeToggle))]
public class DebugModeToggleEditor : Editor{

	public override void OnInspectorGUI()
	{  
		if(GUILayout.Button("Debug")){
			DebugModeToggle.Toggle(true);
		}

		if(GUILayout.Button("Release")){
			DebugModeToggle.Toggle(false);
		}
	} 
}
