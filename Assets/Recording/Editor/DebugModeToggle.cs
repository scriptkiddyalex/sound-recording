﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class DebugModeToggle {

	static List<string> DefinedSymbols{
		get{
			return new List<string>(PlayerSettings.GetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android).Split(new char[1]{(char)59}));
		}
	}

	public static void Toggle(bool value){
		string symbolsLine = "";
		List<string> definedSymbols = DefinedSymbols;

		if(value){
			if(definedSymbols.Contains(DefineSymbols.RECORDING_DEBUG)){
				return;
			}else{
				definedSymbols.Add(DefineSymbols.RECORDING_DEBUG);
			}
		}else{
			if(definedSymbols.Contains(DefineSymbols.RECORDING_DEBUG)){
				definedSymbols.Remove(DefineSymbols.RECORDING_DEBUG);
			}else{
				return;
			}
		}

		int i=0;
		while(definedSymbols.Count > 0 && i != definedSymbols.Count){
			symbolsLine += definedSymbols[i++] + ";";
		}

		PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android, symbolsLine);
	}


	[MenuItem("Tools/Toggle debug &d")]
	private static void ToggleDebug(){
		DebugModeToggle.Toggle(true);
	}

	[MenuItem("Tools/Toggle release &r")]
	private static void ToggleRelease(){
		DebugModeToggle.Toggle(false);
	}

	public static bool IsDebugging{
		get{
			return DefinedSymbols.Contains(DefineSymbols.RECORDING_DEBUG);
		}
	}
}
