﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundTools { 
	
	public static float MaxSampleVolumeOf(float[] samples)
	{
		float soundLevel = 0;
		for (int i = 0; i < samples.Length; ++i)
		{
			float wavePeak = samples[i];
			if (soundLevel < wavePeak)
			{
				soundLevel = wavePeak;
			}
		}
		soundLevel = (float)Math.Round(soundLevel, 2);

		return soundLevel;
	}

	public static float Average(IList<float> list)
	{
		float sum = 0.0f;
		float result = 0.0f;

		for (int i = 0; i < list.Count; i++)
		{
			sum += list[i];
		}

		result = sum / list.Count; 
		return result;
	}

	public static float MaxLevel(IList<float> list)
	{
		float soundLevel = 0;
		for (int i = 0; i < list.Count; ++i)
		{
			float wavePeak = list[i];
			if (soundLevel < wavePeak)
			{
				soundLevel = wavePeak;
			}
		}
		soundLevel = (float)Math.Round(soundLevel, 2); 
		return soundLevel;
	} 
		 
}
