﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using UniRx;
using UnityEngine.UI;

public class RecordingIndicator : MonoBehaviour {
	RecordManager _recordManager;
	Image _image;

	[Inject]
	public void Construct(RecordManager recordManager){
		_recordManager = recordManager;
		_image = this.GetComponent<Image>();

		_recordManager.State.Subscribe(state=>{
			if(state == EarState.ReadyToListen){
				_image.color = Color.red;
			}else{
				_image.color = Color.grey;
			}
		}
		);
	}


}
