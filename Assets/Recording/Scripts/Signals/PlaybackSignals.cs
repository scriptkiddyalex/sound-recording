﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class PlaybackSignals
{

	public class PlaybackEndSignal : Signal<PlaybackEndSignal>
	{
	}

	public class PlayRecordSignal : Signal<AudioClip, PlayRecordSignal>
	{
	}

	public class SpeechPlayDataSignal:Signal<float[], SpeechPlayDataSignal>{}
}