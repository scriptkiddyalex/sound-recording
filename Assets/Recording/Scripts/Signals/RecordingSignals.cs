﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using UniRx;

public class RecordingSignals {
	public class RecordStartedSignal: Signal<IObservable<float[]>, RecordStartedSignal>{}
}
