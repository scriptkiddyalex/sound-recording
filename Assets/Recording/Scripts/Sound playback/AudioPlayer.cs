﻿using System;
using UnityEngine;
using Zenject;
using UniRx;

public class AudioPlayer : MonoBehaviour
{ 
    AudioClip _clip;
    AudioSource _source;
	PlaybackSignals.PlaybackEndSignal _playbackEndSignal;
	PlaybackSignals.SpeechPlayDataSignal _speechPlayDataSignal;
	IDisposable _playUpdateStream;

	[Inject]
	public void Construct(AudioClip clip, PlaybackSignals.PlaybackEndSignal playbackEndSignal,
							PlaybackSignals.SpeechPlayDataSignal speechPlayDataSignal){
		_clip = clip;
		_playbackEndSignal = playbackEndSignal;
		_speechPlayDataSignal = speechPlayDataSignal;

		_source = this.GetComponent<AudioSource>();
		_source.clip = _clip;
	}

	public void Play(AudioClip clip)
	{
		SetClip = clip;
		Play();
	}

	public void Stop()
	{
		_source.Stop();
		_playUpdateStream.Dispose();
	}

	public void Replay()
	{
		_source.Play();
	}
 
    AudioClip SetClip {
        set
        {
            _clip = value;
        }
    }

	public AudioSource Source
	{
		get
		{
			return _source;
		}
	}


    void Play()
    {
        _source.clip = _clip;
		_source.Stop();
        _source.Play();

		_playUpdateStream = Observable
							.EveryUpdate()
							.Take(TimeSpan.FromSeconds(_source.clip.length))
							.Where(update => _source.isPlaying)
							.Subscribe(next => {}, error => {}, PreventPlayback);
    }
		
	//TODO: Add check for unexpected Idle while playing clip
	void Update() { 
		if (_source.timeSamples == _source.clip.samples)
        {
			PreventPlayback();
		}else
		{
			float[] samples = new float[64];
			_source.GetSpectrumData(samples, 0, FFTWindow.BlackmanHarris);
			_speechPlayDataSignal.Fire(samples);
		}
    }

	void PreventPlayback(){
		Stop();
		SendPlaybackEndEvent();
	}

    void SendPlaybackEndEvent()
    {
		_playbackEndSignal.Fire();
    }

    void Dispose()
    {
        GameObject.Destroy(_source.gameObject);
    }
}