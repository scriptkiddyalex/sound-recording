﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using Zenject;
using System;

public enum MouthState{
	ReadyToSpeak,
	Busy
} 

public class SoundManager:IInitializable {
	AudioPlayer _player;

	PlaybackSignals.PlayRecordSignal _playRecordSignal;
	PlaybackSignals.PlaybackEndSignal _playbackEndSignal;
	ReactiveProperty<MouthState> _state;

	public SoundManager(AudioPlayer player, PlaybackSignals.PlayRecordSignal playRecordSignal, PlaybackSignals.PlaybackEndSignal playbackEndSignal)
	{
		_player = player;
		_playbackEndSignal = playbackEndSignal;
		_playRecordSignal = playRecordSignal;
		_state = new ReactiveProperty<MouthState>();
	}
	
	public void Initialize()
	{
		_playbackEndSignal.Listen(SpeechEnd);
		_playRecordSignal.Listen(PlaySpeech); 
		
		ChangeState(MouthState.ReadyToSpeak);
	}

	void PlaySpeech(AudioClip clip)
	{
		_player.Play(clip);
		ChangeState(MouthState.Busy);
	}

	void SpeechEnd(){
		ChangeState(MouthState.ReadyToSpeak);
	}

	void ChangeState(MouthState state){
		_state.Value = state;
	}

	public ReactiveProperty<MouthState> State{
		get{
			return _state;
		}
	}
}
