﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ServiceMessage  {
    public static string NO_DEVICE = "No recording device found!";
    public static string DEVICE_FOUND = "Device found!";
    public static string RECORD_STOP = "Record stop";
    public static string RECORD_START = "Record start";
    public static string RECORD_PLAY = "Record play"; 
}
