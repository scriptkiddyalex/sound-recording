﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using UniRx;
using MashaGame.Services;

public enum EarState{
	ReadyToListen,
	Busy
}

public class RecordManager : IInitializable {
    readonly ReactiveProperty<EarState> _state;
	PlaybackSignals.PlaybackEndSignal _playbackEndSignal;
    readonly SoundManager _soundManager;
    readonly RoomController _roomController;

	public RecordManager(RoomController roomController, PlaybackSignals.PlaybackEndSignal playbackEndSignal, SoundManager soundManager){
		_roomController = roomController;
		_playbackEndSignal = playbackEndSignal;
		_soundManager = soundManager;
		_state = new ReactiveProperty<EarState>();
	}

	public void Initialize(){ 
        ChangeState(EarState.ReadyToListen);
		_roomController
			.ActiveRoom
			.Where(room => room == RoomController.RoomType.Game)
			.Subscribe(room =>
				{
					_soundManager.State
						.Where(state => state == MouthState.ReadyToSpeak)
						.Subscribe(state => ChangeState(EarState.ReadyToListen));

					_soundManager.State
						.Where(state => state == MouthState.Busy)
						.Subscribe(state => ChangeState(EarState.Busy));
				}
			); 
	}

	public ReactiveProperty<EarState> State{
		get{
			return _state;
		}
	}
	 
	void ChangeState(EarState state){
		_state.Value = state;
	}

}
