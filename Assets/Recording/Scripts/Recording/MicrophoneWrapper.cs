﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using UniRx;

public class MicrophoneWrapper: IMicrophoneWrapper, IInitializable{
    string _name;
    int _frequency; 

    public void Initialize()
    {
        _name = Microphone.devices[0];
        _frequency = GetAverageFrequency;
    }

	public void Output(ref AudioClip clip, int sec)
    {
		clip = Microphone.Start(_name, true, sec, _frequency);
    }

	public void Off(){
		Microphone.End(_name); 
	}

	public bool IsRecording{
		get{
			return Microphone.IsRecording(_name);
		}
	}

    public int Position
    {
        get
        {
            return Microphone.GetPosition(_name);
        }
    }

    int GetAverageFrequency
    {
        get
        {
            int minFreq = 0;
            int maxFreq = 0;
            Microphone.GetDeviceCaps(_name, out minFreq, out maxFreq);
            return minFreq + (maxFreq - minFreq) / 2;
        }
    }
}
