using UnityEngine;

public interface IMicrophoneWrapper
{
	void Output(ref AudioClip clip, int sec);
	void Off();
	int Position { get;} 
}