﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using Zenject;

public class Recorder : IInitializable
{
    readonly RecordManager _recordManager;
    readonly IMicrophoneWrapper _microphone;
	AudioClip _clip;

	float[] _samples;
	int _lastMicPosition = 0;
    readonly int _clipLength = 2;

    readonly PlaybackSignals.PlayRecordSignal _playRecordSignal;
    readonly ReactiveProperty<RecordState> _state;
    readonly Subject<float[]> _samplesSubject;
	IDisposable _recordStream;
	IDisposable _updateStream;

	public IObservable<float[]> SampleData
	{
		get
		{
			return _samplesSubject.AsObservable();
		}
	} 
     
	public enum RecordState
	{
		Idle,
		Listen,
		Write
	}

	public Recorder(IMicrophoneWrapper microphone, AudioClip clip, 
		PlaybackSignals.PlayRecordSignal playRecordSignal, RecordManager recordManager)
	{
		_microphone = microphone;
		_clip = clip;
		_playRecordSignal = playRecordSignal;
		_recordManager = recordManager;

		_samples = new float[0];
		_samplesSubject = new Subject<float[]>();
        _recordStream = new Subject<float[]>();
        _state = new ReactiveProperty<RecordState>();
	}


    public void Initialize()
	{ 
        State = RecordState.Idle;

		_recordManager.State
			.Subscribe(state =>{
				if(state == EarState.ReadyToListen){
					Activate();
				}else{
					Deacitvate();
				}
			});
    }

	void Activate()
    {   
		_microphone.Output(ref _clip, _clipLength);

		_updateStream = Observable.EveryUpdate()
			.Where(update => _recordManager.State.Value == EarState.ReadyToListen)
			.Subscribe(update => ProcessMicrophone());

		_recordStream = _samplesSubject
						.SkipUntil(_samplesSubject.Where(samples => SoundTools.MaxSampleVolumeOf(samples) > Config.SOUND_THRESHOLD))
						.Take(60)
						.Subscribe(SaveSamples, error => {}, EjectRecord); 

        _lastMicPosition = 0;

        Debug.Log("Activate");
    }

	void Deacitvate()
	{ 
		_updateStream.Dispose();
		_recordStream.Dispose();

		_samples = new float[0];
		_microphone.Off();

        if (_clip.preloadAudioData)
		{
			_clip = null;
		}
		else
		{
			_clip.SetData(new float[1], 0);
		}

        Debug.Log("Deacitvate");
	}


	void ProcessMicrophone(){ 
		int position = _microphone.Position;
        if (_lastMicPosition > position)
        {
            _lastMicPosition = 0;
        }else if (_lastMicPosition == position)
        {
            return;
        }

        float[] lastSamples = new float[Mathf.Abs(position - _lastMicPosition)]; 

        _clip.GetData(lastSamples, _lastMicPosition);
		_samplesSubject.OnNext(lastSamples);
		_lastMicPosition = position;

        Debug.Log("Process samples");
    }

	 
	void SaveSamples(float[] samples)
	{
		Debug.Log("Saving samples ...");

		int savedSamplesCount = _samples.Length;
		Array.Resize(ref _samples, _samples.Length + samples.Length);
		Array.Copy(samples, 0, _samples, savedSamplesCount, samples.Length);
	}

	void EjectRecord()
	{
		Debug.Log("Eject method");

		var recognizedClip = AudioClip.Create("temp", _samples.Length, _clip.channels, _clip.frequency, false);
		recognizedClip.SetData(_samples, 0);
		_playRecordSignal.Fire(recognizedClip);
	}

	public RecordState State
	{
		get{
			return _state.Value;
		}private set
		{
			_state.Value = value;
		}
	}
}
