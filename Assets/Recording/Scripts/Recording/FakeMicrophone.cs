using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using UniRx;

public class FakeMicrophone : IMicrophoneWrapper, IInitializable
{
    readonly AudioClip _clip;
    readonly AudioSource _source;
	float[] _samples;

	public FakeMicrophone(AudioClip clip, AudioSource source)
	{
		_clip = clip; 
		_source = source;
	}
	
	public void Initialize()
	{
		_samples = new float[_clip.samples * _clip.channels];
		_clip.GetData(_samples, 0);
	}

	public void Output(ref AudioClip clip, int sec)
	{
		clip = _clip;
		_source.clip = _clip;
		_source.Play();
	}

	public void Off()
	{
		_source.Stop();
	}

	public int Position
	{
		get
		{  
			return _source.timeSamples;
		}
	}
	
}