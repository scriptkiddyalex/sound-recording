using System;
using UnityEngine;
using Zenject;

public class SoundRecordInstaller : MonoInstaller<SoundRecordInstaller>
{
	public override void InstallBindings()
	{
		BindAudioTools();
		BindSignals();
		BindManagers();
		BindView();
	}

	void BindManagers()
	{

		Container.BindInterfacesAndSelfTo<RecordManager>()
				 .AsSingle()
				 .NonLazy();

		Container.BindInterfacesAndSelfTo<SoundManager>()
				 .AsSingle()
				 .NonLazy();
	}

	void BindSignals()
	{
		Container.Bind<SignalManager>().AsSingle();

		Container.DeclareSignal<PlaybackSignals.PlayRecordSignal>();
		Container.DeclareSignal<PlaybackSignals.PlaybackEndSignal>();
		Container.DeclareSignal<PlaybackSignals.SpeechPlayDataSignal>();
		Container.DeclareSignal<RecordingSignals.RecordStartedSignal>();
	}

	void BindAudioTools()
	{
		Container.Bind<AudioPlayer>()
			.FromComponentInNewPrefabResource("Prefabs/Audio/AudioHolder").AsSingle();

		Container.Bind<AudioClip>()
			.FromInstance(AudioClip.Create("Moq", 1, 1, 44100, false)); 

#if RECORDING_DEBUG
        Container.Bind<AudioSource>()
                 .FromComponentInNewPrefabResource("Prefabs/Audio/VoiceHolder")
                 .WhenInjectedInto<FakeMicrophone>();

        Container.Bind<AudioClip>()
                 .FromResource("Samples/FemaleVoiceSample")
                 .WhenInjectedInto<FakeMicrophone>();

        Container.BindInterfacesAndSelfTo<FakeMicrophone>()
                 .AsSingle();
#else
		Container.BindInterfacesAndSelfTo<MicrophoneWrapper>()
		         .AsSingle();
#endif

        Container.BindInterfacesAndSelfTo<Recorder>()
            .AsSingle()
			.NonLazy();
    }

	void BindView(){
	
	}
	 
}