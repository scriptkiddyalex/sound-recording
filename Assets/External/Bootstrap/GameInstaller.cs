using UnityEngine;
using Zenject;
using MashaGame.Services;

public class GameInstaller : MonoInstaller<GameInstaller>
{
    public override void InstallBindings()
    {
		Container.Bind<RoomController>().AsSingle();
    }
}