﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
namespace MashaGame.Services
{
	public class RoomController
	{
		public enum RoomType
		{
			Game, Eat, Bath, Night, Wardrobe, Makeup
		}

		private readonly ReactiveProperty<RoomType> _activeRoom;

		public RoomController()
		{
			_activeRoom = new ReactiveProperty<RoomType>();
		}

		public ReactiveProperty<RoomType> ActiveRoom
		{
			get { return _activeRoom; }
		}
	}
}