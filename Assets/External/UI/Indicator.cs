﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class Indicator : MonoBehaviour {
    public Text label;
    public float ascendSpeed = 10;
    public float descendSpeed = 3;
    Image _image; 

    void Init()
    {
        _image = GetComponent<Image>();
        _image.fillMethod = Image.FillMethod.Vertical;
    }

    public void SetValue(float value)
    {
        label.text = value.ToString();
        float speed = 0;
        if (value > Image.fillAmount)
        {
            speed = ascendSpeed;
        }
        else
        {
            speed = descendSpeed;
        }
        Image.fillAmount = Mathf.Lerp(Image.fillAmount, value, Time.deltaTime * speed);
    }

    Image Image
    { 
        get
        {
            if (!_image)
            {
                Init();
            }

            return _image;
        }
    }
}
