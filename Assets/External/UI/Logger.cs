﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(GUIText))]
public class Logger : MonoBehaviour {
    Text _textField;

	void Awake () {
        Init();
	}
	
    void Init()
    {
        if (!_textField)
        {
            _textField = this.GetComponent<UnityEngine.UI.Text>(); 
        }
    }

	void Update () {
		
	}

    public void Set(string text)
    {
        Init();
        _textField.text = text;
    }

    public void Add(string text)
    {
        Init();
        _textField.text += "\n";
        _textField.text += text;
    }
}
