﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Toggle))]
public class ToggleIndicator : MonoBehaviour {
    Toggle _toggle;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void Init()
    {
        _toggle = GetComponent<Toggle>();
    }

    public void Switch(bool value)
    {
        Toggle.isOn = value;
        //if ((!Toggle.isOn && value)|| (!Toggle.isOn && !value))
        //{
        //    Toggle.Select(); 
        //}else if(Toggle.isOn && !value)
        //{
        //    Toggle.Select(); 
        //}
    }

    Toggle Toggle
    {
        get
        {
            if (!_toggle)
            {
                Init();
            }

            return _toggle;
        }
    }
}
